import '../scss/main.scss';

const selectedSection = /do\=([^&]+)/.exec(location.search)?.[1]

const sectionHandlers = {
  // 'plan9': () => {
  //   activateSection("vm_container")
  //   console.log('booting plan9');
  //   import(/* webpackChunkName: "plan9" */ './plan9')
  // },
  // 'winme': () => {
  //   activateSection("vm_container")
  //   console.log('booting Windows Millennium Edition');
  //   import(/* webpackChunkName: "winme" */ './winme')
  // },
  // 'sierrah': () => {
  //   console.log('booting sierrah');
    
  // }
}

function activateSection(id: string) {
  const elements = document.querySelectorAll("main > section");
  elements.forEach(e => e.id == id ? e.setAttribute('active', '') : e.removeAttribute('active'))
}

function disableButton(action?: string) {
  action = action ? "/?do=" + action : '/'
  document.querySelectorAll('nav [buttons] a').forEach(e => {
    if (e.getAttribute('href') == action) {
      e.setAttribute('disabled', '')
    }
  })
}

disableButton(selectedSection);
if (selectedSection) {
  const handler = sectionHandlers[selectedSection]
  if (!handler) {
    console.error("no handler for", selectedSection);
  } else {
    handler();
  }
}