function cobs_decode(input) {
  let result = []
  let offset_value = 0;
  let last_offset_index = 0;

  for (let i = 0; i < input.length; i++) {
    let byte = input[i];
    if (byte == 0) {
      break
    }

    if (offset_value == (i + last_offset_index)) {
      offset_value = byte
      byte = 0
      last_offset_index = i
    }

    if (i != 0) {
      result.push(byte)
    }
  }
  result.pop();
  result.pop();
  result.pop();
  result.pop();

  console.log(result)
  return result;
}