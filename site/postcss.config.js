module.exports = {
  plugins: {
    'cssnano': {
      preset: ['advanced', {
        discardComments: {
          removeAll: true,
        },
      }],
    },
    'postcss-preset-env': {
      browsers: ['>1%', 'last 4 versions', 'Firefox ESR'],
    },
  }
}