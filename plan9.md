# boot

```sh
boot from fossil
user=glenda
$ vgasize=1280x1024x16
$ aux/vga -l $vgasize
$ rio -i riostart
# right click on gray background > new, draw box with right mouse button
$ page cv.png
```

# mount cd and copy files
```sh
9660srv
mount /srv/9660 /n/dist /dev/sdD0/data
cp /n/dist/cv.png /usr/glenda
```

# set startup apps

```sh
acme
# right click on bin/rc/riostart
window 10,10,1100,1000 page cv.png
# middle click put
```